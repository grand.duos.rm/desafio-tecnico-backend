# Documentação do Teste Técnico - Desenvolvimento de API em Node/TypeScript

_Desafio técnico para o processo seletivo Let's Code_<br>
_Author: Rafael Medeiros_


 ## Tecnologia

Typescript + NodeJS + Express

 ## Execução

Para executar o projeto (desenvolvimento)

```
> cd desafio-tecnico-backend
> npm install
> docker-compose.yml up
> npm run migration:run
> npm run start:dev
```
 
 Para executar o projeto (produção)

```
> cd desafio-tecnico-backend
> npm install
> docker-compose.yml up
> npm run migration:run
> npm run build
> npm run start:prod
```
 
 
 ## Esclarecimentos

No projeto optei por utilizar um banco de dados "real", adicionei o docker-compose, portanto, é necessário a utilização do docker no equipamento para o ambiente de desenvolvimento e o banco em questão tomei como preferência o PostgreSQL, na qual tenho maior familiaridade em sua utilização.
Conforme documentação utilizado para o desenvolvimento, seguir as regras de negócio e também a tratativas para os endpoints correspondentes.
As variaveis de ambiente encontram-se em .env.example onde contém as variaveis na qual utilizei na API, para sua utilização basta remover o .example da sequência.






