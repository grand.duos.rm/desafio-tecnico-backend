\connect desafiobackend

CREATE TABLE users (
    id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
    login VARCHAR ( 50 ) UNIQUE NOT NULL,
    password VARCHAR ( 50 ) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP 
);

insert into users(login,password,created_at) values ('letscode','lets@123',now())