import { Entity, Column } from "typeorm";
import { DefaultEntity } from "../modules/common/shared/entities";
import { AuthModel } from "../modules/users/auth/models";

@Entity("users")
export class Users extends DefaultEntity implements AuthModel {
  @Column()
  login: string;

  @Column()
  password: string;
}
