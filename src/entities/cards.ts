import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { CardModel } from "../modules/card/models";
import { ListType } from "../modules/common/shared/constants";

@Entity("cards")
export class Cards implements CardModel {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  titulo: string;

  @Column()
  conteudo: string;

  @Column({ type: "enum", enum: ListType })
  lista: ListType;
}
