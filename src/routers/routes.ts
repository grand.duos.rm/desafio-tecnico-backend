import { ok } from "../helpers/responseHelpers";
import { Router } from "express";
import { sysReady } from "../helpers/messageHelper";
import { authController } from "../modules/users/auth";
import { cardController } from "../modules/card";
import authMiddleware from "../middleware/authMiddleware";

const live = (_: any, res: any): any => ok(res, sysReady("live"));
const routes = Router();

routes.get("/live", live);

routes.post("/login", (request, response) => {
  return authController.handleRequest(request, response);
});

routes.get("/cards", authMiddleware, (request, response) => {
  return cardController.getRequest(request, response);
});

routes.post("/cards", authMiddleware, (request, response) => {
  return cardController.createRequest(request, response);
});

routes.put("/cards/:id", authMiddleware, (request, response) => {
  return cardController.updateRequest(request, response);
});

routes.delete("/cards/:id", authMiddleware, (request, response) => {
  return cardController.deleteRequest(request, response);
});

export default routes;
