export function normalizePort(val: string, defaultPort: number): number {
  const port = Number.parseInt(val, 10);
  if (Number.isNaN(port)) {
    return defaultPort;
  }
  if (port >= 0) {
    return port;
  }
  return defaultPort;
}
