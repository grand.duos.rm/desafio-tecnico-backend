import { MigrationInterface, QueryRunner, Table } from "typeorm"

export class cards1665700710564 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "cards",
            columns: [
                { 
                    name: 'id',
                    type: 'varchar',
                    isPrimary: true,
                    generationStrategy: 'uuid',
                    default: 'uuid_generate_v4()',
                },
                {
                    name: "titulo",
                    type: "varchar",
                    length: "125",
                    isNullable: false,
                    isUnique: true
                },
                {
                    name: "conteudo",
                    type: "varchar",
                    length: "2000",
                    isNullable: false,
                    isUnique: false
                }, 
                {
                    name: "lista",
                    type: "varchar",
                    length: "5",
                    isNullable: false,
                    isUnique: false
                },  
            ] 
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("cards")
    }

}
 