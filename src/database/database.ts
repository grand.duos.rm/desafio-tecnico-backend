import { DataSource } from "typeorm";
import 'reflect-metadata' 
import dotenv from 'dotenv';
dotenv.config();

const port: any = process.env.POSTGRES_PORT
const AppDataSource = new DataSource({
    type: "postgres",
    host: process.env.POSTGRES_HOST,
    port: port, 
    username: process.env.POSTGRES_USER,
    password :  process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    entities: ["./src/entities/**/*.ts"],
    migrations: ["./src/database/migrations/*.ts"],
    synchronize: false,
    logging: false,
})
export default AppDataSource
