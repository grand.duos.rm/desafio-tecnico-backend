import { DefaultModel } from "../../../common/shared/models";

export type AuthModel = DefaultModel & {
  login: string;
  password: string;
};
