import { AuthController } from "./controllers"
import { AuthService } from "./services"

 

const authService = new AuthService()
const authController = new AuthController(authService) 

export { authController }