import AppDataSource from "../../../../database/database";
import { Users } from "../../../../entities/users";
import { GenerateToken } from "../../../../provider/generate-token";

export class AuthService {
  async login({ login, password }: any) {
    var boolean = false;
    try {
      const user = await AppDataSource.getRepository(Users).findOne({
        where: { login },
      });
      if (!user) {
        return [boolean];
      }

      const isValidPassword = (await password) === user.password ? true : false;
      if (!isValidPassword) {
        return [boolean];
      }

      const generateToken = new GenerateToken();

      const token = await generateToken.execute(user.id);

      let credential = user.login;
      boolean = true;
      return [boolean, credential, token];
    } catch (error) {
      const erro = error as string;
      return [boolean, erro];
    }
  }
}
