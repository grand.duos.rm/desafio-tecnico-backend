import { Request, Response } from "express";
import { AuthService } from "../services/auth.service";
import "reflect-metadata";
import dotenv from "dotenv";
dotenv.config();

export class AuthController {
  constructor(private authService: AuthService) {}

  async handleRequest(request: Request, response: Response) {
    const { DEFAULT_LOGIN, DEFAULT_PASSWORD } = process.env;

    try {
      const retorno = await this.authService.login({
        login: DEFAULT_LOGIN,
        password: DEFAULT_PASSWORD,
      });

      return retorno[0]
        ? response
            .status(200)
            .json({ credential: retorno[1], token: retorno[2] })
        : response.status(401).json({ message: "unauthorized access" });
    } catch (error) {
      return response.status(500).json({ message: "Unexpected error" });
    }
  }
}
