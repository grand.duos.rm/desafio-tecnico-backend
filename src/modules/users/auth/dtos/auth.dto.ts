export interface IAuthDTO {
  login: string;
  password: string;
}
