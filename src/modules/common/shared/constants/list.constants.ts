export enum ListType {
  Novo = "Novo",
  ToDo = "To Do",
  Doing = "Doing",
  Done = "Done",
}
