import { CardController } from "./controllers";
import { CardService } from "./services";

const cardService = new CardService()
const cardController = new CardController(cardService) 

export { cardController }