import { Request, Response } from "express";
import { CardService } from "../services";

export class CardController {
  constructor(private cardService: CardService) {}

  async getRequest(request: Request, response: Response) {
    try {
      const getReturn = await this.cardService.find();
      return getReturn.length != 0
        ? response.status(202).json(getReturn)
        : response.status(404).json({ message: "not found" });
    } catch (error) {
      return response.status(500).json({ message: "unexpected error." });
    }
  }

  async createRequest(request: Request, response: Response) {
    const data = request.body;
    try {
      const result = await this.cardService.create(data);

      return response.status(201).json(result);
    } catch (error) {
      return response.status(500).json({ message: error });
    }
  }

  async updateRequest(request: Request, response: Response) {
    const id = request.params.id;
    const data = request.body;

    try {
      const result = await this.cardService.update(id, data);

      return result
        ? response.status(202).json(result)
        : response.status(404).json({ message: "failure params" });
    } catch (error) {
      return response.status(400).json({ message: "failure params" });
    }
  }

  async deleteRequest(request: Request, response: Response) {
    const id = request.params.id;

    try {
      const result = await this.cardService.delete(id);

      return result
        ? response.status(202).json(result)
        : response.status(404).json({ message: "failure params" });
    } catch (error) {
      return response.status(404).json({ message: "failure params" });
    }
  }
}
