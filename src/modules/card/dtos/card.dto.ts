import { ListType } from "../../common/shared/constants";

export interface ICardDTO {
  titulo: string;
  conteudo: string;
  lista: ListType;
}
