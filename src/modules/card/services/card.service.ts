import AppDataSource from "../../../database/database";
import { Cards } from "../../../entities/cards";
import { ICardDTO } from "../dtos";

const getRepository = AppDataSource.getRepository(Cards);

export class CardService {
  async create(data: ICardDTO) {
    const stetament = await getRepository.create(data);
    return await await getRepository.save(stetament);
  }

  async find() {
    try {
      return await getRepository.find();
    } catch (error) {
      const erro = error as string;
      return erro;
    }
  }

  async update(id: any, data: ICardDTO) {
    const card = await AppDataSource.getRepository(Cards).findOne({
      where: { id },
    });
    if (!card) {
      return false;
    }

    const values = { id, ...data };
    return await getRepository.save(values);
  }

  async delete(id: any) {
    const card = await AppDataSource.getRepository(Cards).findOne({
      where: { id },
    });
    if (!card) {
      return false;
    }
    await getRepository.delete(id);

    return this.find();
  }
}
