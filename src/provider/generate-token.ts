import jwt from 'jsonwebtoken'
import 'reflect-metadata' 
import dotenv from 'dotenv';
dotenv.config();

class GenerateToken {

    async execute(userId: string) {
        const JWT_SECRET: any  = process.env.JWT_SECRET
        const token = jwt.sign(
            { id: userId },
            JWT_SECRET,
            { expiresIn: '1h' }
        ) 

        return token
    }
}

export { GenerateToken }