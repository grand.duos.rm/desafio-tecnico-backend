import 'reflect-metadata'
import express from 'express';  
import https from 'https';
import http from 'http';
import routes from './routers/routes';  
import AppDataSource from './database/database';

export default class Server {
  public express: express.Application;


  constructor() { 
    this.express = express();
    this.middlewares();
    this.connectDB();
    this.routes(); 
  }

  // Criar um servidor https se houver necessidade
  // Podemos utilizar um nginx na frente para isso também, até mais fácil
  public startHttps(port: number): void {
 
    const httpsOptions = {
      cert: '', 
      key: '',
    };
    const server = https.createServer(httpsOptions, this.express);
    server.listen(port, () => {
      console.log(`Server started on port ${port}`);
    });

  }  

   // Implementar os mecanismos para criar uma conexão com o banco de dados
   public async connectDB(): Promise<any> {
    AppDataSource.initialize().then(() => {
      console.log("The database has been initialized")
    }).catch((err) => {
      console.error("An error occurred while initializing the database", err)
    })

  }

  
  // Implementar o servidor padrão https sem a necessiade das public and privates keys
  public startHttp(port: number): void {
    const server = http.createServer(this.express);
    server.listen(port, () => {
      console.log('Server is running :', port);
    });
  }

  // Adicionar os middlewares
  private middlewares(): void { 
    this.express.use(express.json({ limit: '10mb' }));
    this.express.use(express.urlencoded({ extended: true }));  
  }
 
  private routes(): void {
    this.express.use('/', routes); 
  }
}
