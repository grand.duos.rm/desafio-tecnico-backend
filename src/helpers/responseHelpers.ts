import { Response } from 'express';

export const ok = (res: Response, content: any): Response => res.status(200).json({
  code: 'OK',
  content,
});
