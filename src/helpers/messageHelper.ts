export const sysReady = (option: string): string => {
  switch (option) {
    case 'live': return 'We are Up and Running.. YEAH!';
    case 'healthy db': return 'Our Database looks 100% Healthy. Prove me Wrong xP.';
    default: return 'I think we\'re On..but I can be Wrong xD.';
  }
};
