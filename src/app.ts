import Server from './server';
import { normalizePort } from './util/functions';


console.log(new Date().toString(), 'Iniciando Servidor');
const server = new Server();

server.startHttp(normalizePort(process.env.PORT || '', 5000)); 

export const app = server.express; 